from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests


def get_photo(query):
    """
    Get the URL of a picture form the Pexels API.
    """

    url = f"https://api.pexels.com/v1/search?query={query}"

    headers = {
        "Authorization": PEXELS_API_KEY,
    }

    response = requests.get(url, headers=headers)
    api_dict = response.json()
    return api_dict["photos"][0]["src"]["original"]


def get_weather(city, state):
    params = {
        "q": f"{city} {state}",
        "appid": OPEN_WEATHER_API_KEY,
    }

    geocode_url = "http://api.openweathermap.org/geo/1.0/direct"
    geocode_response = requests.get(geocode_url, params=params)
    geocode_dict = geocode_response.json()
    try:
        latitude = geocode_dict[0]["lat"]
        longitude = geocode_dict[0]["lon"]
    except (KeyError, IndexError):
        return None

    params = {
        "lat": latitude,
        "lon": longitude,
        "appid": OPEN_WEATHER_API_KEY,
        "units": "imperial",
    }
    weather_url = "https://api.openweathermap.org/data/2.5/weather"
    weather_response = requests.get(weather_url, params=params)
    weather_dict = weather_response.json()
    try:
        return {
            "temp": weather_dict["main"]["temp"],
            "description": weather_dict["weather"][0]["description"],
        }
    except (KeyError, IndexError):
        return None
